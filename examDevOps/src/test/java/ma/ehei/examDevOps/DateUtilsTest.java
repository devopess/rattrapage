package ma.ehei.examDevOps;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;

public class DateUtilsTest {
    DateUtils dateUtils;

    @Before
    public void setUp()
	{
		dateUtils=new DateUtils();
	}
    
    @Test
    public void ajoutJoursSucces()
    {
        Date expected=dateUtils.ajoutJours("10/04/2023", 2);
        Date actual= DateTime.parse("12/04/2023", DateTimeFormat.forPattern("dd/MM/yyyy")).toDate();
        assertEquals(expected, actual);
    }

    @Test(expected = Exception.class)
    public void ajoutJoursExcetion()
    {
        dateUtils.ajoutJours("10/04/2023", -5);
    }
}
