package ma.ehei.examDevOps;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class DateUtils {

	public Date ajoutJours(String dateString, int nbrJour) {
		DateTime date = DateTime.parse(dateString, DateTimeFormat.forPattern("dd/MM/yyyy"));
		if (nbrJour < 0) {
			throw new IllegalArgumentException("Le parametre nbrJour doit etre supperiere a Zero");
		}
		return date.plusDays(nbrJour).toDate();
	}

}